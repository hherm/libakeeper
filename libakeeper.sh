#!/bin/bash
# get photos, status of camera and e-mail home
# the core script, triggered by cronjob
# no need to run as root.

thumbnailpath="/tmp/thumbnails"
mailto="email"
mailsubject="subject"
memoryFile=~/libakeeper.dat

# check if camera is there
cameraresult=$( gphoto2 --auto-detect | awk '/^Nikon/ || /^Canon/ || /^Panasonic/')
if [ -z "${cameraresult}" ]; then
    echo "Error: No camera detected. Aborting."
    exit 1
    # mail "no camera detected"
else
    echo "Camera: $cameraresult"   
fi

# get camera storage info
camerastatus=$(gphoto2 --storage-info | awk '/free/ || /total/ {print}' | awk 'FNR <=2')
echo "got status."

# get last image name
## get last folder

### differences between versions 5.20. and 5.23: dilimiter character
# for 5.20 use: folderlist=$(gphoto2 -l | awk '/\/DCIM\//{print substr($NF,2,29)}')
folderlist=$(gphoto2 -l | awk '/\/DCIM\//{print $NF}' | cut -d\' -f 2)        # this takes a while...
imgPath=$(echo $folderlist | awk '{print $NF}')
lastImg=$(gphoto2 -f=$imgPath -n | awk '{print $NF}')
echo "got lastImg: "$lastImg" from "$imgPath

# get the last image's thumbnail
if [ -d "$thumbnailpath" ]; then
    cd $thumbnailpath
    echo "Changed dir to: "$thumbnailpath
else
    mkdir $thumbnailpath
    cd $thumbnailpath
    echo "Created dir."
fi

# avoid thmbnail collision
if [ "$(ls -A $thumbnailpath)" ]; then
    echo "cleaning up $thumbnailpath."
    rm $thumbnailpath/*
else
    echo "$thumbnailpath is clean."
fi
imgName=$(gphoto2 -f=$imgPath --get-thumbnail=$lastImg | awk '{print $NF}')
echo "got imgName."

# remember the last image 
if [ -f "$memoryFile" ]; then
    oldImg=$(cat $memoryFile)
    oldNumber=$(echo $oldImg | awk -F_ '{print substr($4,1,4)}')
    newNumber=$(echo $imgName | awk -F_ '{print substr($3,1,4)}')
    ((newImgNumber=newNumber-oldNumber))
    if [ $newImgNumber -eq 1 ]; then
        newPicturesText="There is "$newImgNumber" new picture."
    else
        newPicturesText="There are "$newImgNumber" new pictures."
    fi
    echo $newPicturesText
else
    echo "No inforamion about last picture, but memoryfile created. ($memoryFile)"
fi
echo "$imgPath/$imgName" > $memoryFile    

# mail the status
## compose body
mailbody="Storage: $camerastatus"
echo $mailbody" - "$imgName - $newPicturesText
#| mutt -s $mailsubject -a "$thumbnailpath/$imgName" -- $mailto

# done.
