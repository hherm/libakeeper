#!/bin/bash
# get camera photos and send the number of pictures plus preview of the last via network (mobile in the future)
# install and test script
# author: hherm
# licence: GPLv3
# written: 2020-05-29
# last modified: 2020-06-02


# we need root for setup
if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
fi

# make shure everything we need is installed
installthis=""
command -v gphoto2 >/dev/null 2>&1 || { echo >&2 "I require gphoto2 but it's not installed.  Marked for installation."; installthis=$installthis" gphoto2";}
command -v postfix >/dev/null 2>&1 || { echo >&2 "I require postfix but it's not installed.  Marked for installation."; installthis=$installthis" postfix";}
command -v mutt >/dev/null 2>&1 || { echo >&2 "I require mutt but it's not installed.  Marked for installation."; installthis=$installthis" mutt";}


if ! [ -z "${installthis}" ]; then
	# there are programs to install, here we go
	sudo apt update && sudo apt install -y $installthis
fi

# create memory folder
memoryPath=~/.libakeeper
if ! [ -f "$memoryPath" ]; then
    mkdir $memoryPath
fi

# what about a small config file?

# configure email

# add cron-job
#  we need a worker-script too
#    tasks are: find latest image, compare with last one, if no changes, take a picture, connect to mobile, email the picture, go to sleep

# test email
