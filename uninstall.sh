#!/bin/bash
# get camera photos and send the number of pictures plus preview of the last via network (mobile in the future)
# uninstall and remove all files we are responsible for
# author: hherm
# licence: GPLv3
# written: 2020-06-02
# last modified: 2020-06-03


# we need root for this
if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
fi

# we should uninstall all programs we installed
uninstallthis="gphotofs gphoto2"
sudo apt remove -y $uninstallthis

mountscriptdst="/usr/local/bin/mountcamera.sh"

# delete mount location
printf "removing mount location..."
cameramountpoint=$(cat $mountscriptdst | grep 'MOUNTPOINT=' | cut -d '"' -f 2 -)
if [[ -d ${cameramountpoint} ]]; then 
	rmdir $cameramountpoint
    echo "done."
else
    echo "missing."
fi

# delete mount script
printf "removing mount script..."
if [[ -f ${mountscriptdst} ]]; then
    rm $mountscriptdst
    echo "done."
else
    echo "missing."
fi

# removing udev rule
printf "removing udev-rule..."
udevpath="/etc/udev/rules.d"
udevrulename="71-mountcamera.rules"
udevrulepath=$udevpath"/"$udevrulename
if [[ -f ${udevrulepath} ]]; then 
    rm $udevrulepath
    echo "done."
else
    echo "missing."
fi

# remove email settings (but ask for first!)
